#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" Servidor """
import os
import socketserver
import sys


"""
 python3 server.py <IP> <puerto> <fichero_audio>
"""

ERROR = "Usage: python3 server.py <IP> <port> <audio_file>"

CODES = {
    200: f"SIP/2.0 200 OK \r\n %s",
    400: b"SIP/2.0 400 Bad Request",
    405: b"SIP/2.0 405 Method Not Allowed"
}
METHODS = ["INVITE", "ACK", "BYE"]


def find(name, path): #para saber si existe la canción
    for root, dirs, files in os.walk(path):
        if name in files:
            return True


def get_arguments():
    if len(sys.argv) == 4 and find(sys.argv[3], "."): #punto: directorio raíz, se encarga de buscar si existe la canción
        ip = sys.argv[1]
        port = sys.argv[2]
        audio_file = sys.argv[3]
        return ip, int(port), audio_file
    else:
        sys.exit(ERROR) #si no existe, error


class EchoHandler(socketserver.DatagramRequestHandler):
    """ Sever class """

    sessions = {} #creamos diccionario

    def handle(self):
        """ Handle process """
        try:
            request = self.rfile.read().decode("utf-8") # recojo lo que me llega del cliente
            method_session, head, length, body = request.split("\r\n")
            method, sip, sip_constant = method_session.split()
            receptor, ip = sip.split(":")[1].split("@")
            assert method in METHODS # vemos si el método existe
            assert sip_constant, "SIP/2.0"
            if method in METHODS: # en caso de que el método exista
                if method == METHODS[0]:
                    origin_address, ip = body.split("\n")[2].split("=")[1].split()
                    self.sessions[origin_address] = ip
                    print(self.sessions)
                    self.wfile.write(bytes(CODES[200] % body, encoding="utf-8"))
                elif method == METHODS[1]:
                    print("Enviando fichero de audio:")

                else:
                    self.wfile.write(bytes("Servidor Finalizado", encoding="utf-8"))
                    sys.exit("====Finalizando servidor====") #el metodo que queda, bye
            else:
                self.wfile.write(CODES[405])
        except Exception as e:
            self.wfile.write(CODES[400]) #si el metodo no existe


if __name__ == '__main__':
    ip, port, audio_file = get_arguments()
    print("Listening...")
    socketserver.UDPServer((ip, port), EchoHandler).serve_forever() #server siempre activo escuchando
