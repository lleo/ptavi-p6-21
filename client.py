#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" Cliente que abre un socket a un servidor """

import socket
import sys

"""
 python3 client.py <metodo> <receptor>@<IPreceptor>:<puertoSIP>

"""

ERROR = "Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>"


def get_arguments():
    try:
        method = sys.argv[1] #método
        client_parameters = sys.argv[2] #el resto de argumentos
        return method, client_parameters
    except Exception:
        sys.exit(ERROR)


def get_client_parameters(client_parameters):
    try:
        receptor = client_parameters.split("@")[0] #uso la @ para separar y me quedo con el primero argumento
        ip_receptor, puerto_sip = client_parameters.split("@")[1].split(":") #el resto de argumentos
        return receptor, ip_receptor, int(puerto_sip)
    except Exception:
        sys.exit(ERROR) #lanzamos error


def invite_method(receptor, ip_receptor):
    method = "INVITE sip:" + receptor + "@" + ip_receptor + " SIP/2.0\r\n"
    head = "Content-Type: application/sdp\r\n"
    body = "\nv=0\no=lc@gmail.com 127.0.0.1\ns=lucia_session\nt=0\nm=audio 1234 RTP" #pongo body antes que length para que lo lea
    length = f"Content-Length: {len(body)}\r\n"
    return method + head + length + body


def ack_method(receptor, ip_receptor):
    method = "ACK sip:" + receptor + "@" + ip_receptor + " SIP/2.0\r\n"
    head = "Content-Type: application/sdp\r\n"
    body = ""
    length = f"Content-Length: {len(body)}\r\n"
    return method + head + length + body


def bye_method(receptor, ip_receptor):
    body = ""
    method = "BYE sip:" + receptor + "@" + ip_receptor + " SIP/2.0\r\n"
    head = "Content-Type: application/sdp\r\n"
    length = f"Content-Length: {len(body)}\r\n"
    return method + head + length + body


SIP_REQUEST = {
    "INVITE": invite_method,
    "ACK": ack_method,
    "BYE": bye_method
}


def client(metodo, receptor, ip_receptor, puerto_sip):
    try:
        # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            request = SIP_REQUEST[metodo](receptor, ip_receptor) #SIP_REQUEST es el diccionario, donde el valor es una función. Coge método, y como llama a funciones, pongo receptor e ip_receptor que son los parámetros de cada función
            s.connect((ip_receptor, puerto_sip))
            s.send(bytes(request, "utf-8")) #enviamos datos al server
            print(f"Enviado: \n{request} \n")
            server_response = s.recv(1024).decode("utf-8") #recojo lo que me devuelve el server
            print(f"Recibido: \n{server_response} \n")
            if "200" in server_response:
                request_ack = SIP_REQUEST["ACK"](receptor, ip_receptor) #si es 200 enviamos ack
                s.send(bytes(request_ack, "utf-8"))
                print(f"Enviado: \n{request_ack} \n")
                server_response = s.recv(1024).decode("utf-8") #espera respuesta
                print(f"Recibido: \n{server_response} \n")
    except Exception:
        sys.exit(ERROR)


if __name__ == '__main__':
    method, client_parameters = get_arguments() # pasamos los argumentos (metodo por un lado, y direccion, ip,y puerto por otro)
    receptor, ip_receptor, puerto_sip = get_client_parameters(client_parameters)
    client(method, receptor, ip_receptor, puerto_sip)
    print("=====Cliente Finalizado=======")